import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
//Robert Gutkowski 1810515
public class RpsChoice implements EventHandler<ActionEvent>{
    private TextField text;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String playerChoice;
    private RpsGame game;

    public RpsChoice(TextField text, TextField wins, TextField losses, TextField ties, String playerChoice, RpsGame game) {
        this.text = text;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.playerChoice = playerChoice;
        this.game = game;
    }
    @Override

    public void handle(ActionEvent arg0) {
        String result = game.playRound(playerChoice);
        this.text.setText(result);
        this.wins.setText("wins: "+game.getWins());
        this.losses.setText("losses: "+game.getLose());
        this.ties.setText("ties: "+game.getTies());
    }
}
