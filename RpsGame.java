import java.util.Random;
//Robert Gutkowski 1810515
public class RpsGame {
    private int wins;
    private int ties;
    private int lose;
    private Random rand;

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.lose = 0;
        this.rand = new Random();
    }

    public int getWins() {
        return wins;
    }

    public int getTies() {
        return ties;
    }

    public int getLose() {
        return lose;
    }

    public String playRound(String playerChoice){
        int generatedNum = rand.nextInt(3);
        String computerChoice = null;
        String print = null;
        switch(generatedNum){
            case 0:
                computerChoice = "Rock";
                break;
            case 1:
                computerChoice = "Paper";
                break;
            case 2:
                computerChoice = "Scissor";
                break;
        }
        print = rpsResult(playerChoice, computerChoice);
        return print;
    }

    private String rpsResult(String playerChoice, String botChoice) {
        String print;
        if(playerChoice.equals(botChoice)){
            this.ties += 1;
            print = printStatement(botChoice, "neither won, nor lose");
        }
        else if(playerChoice.equals("Rock")){
            if(botChoice.equals("Paper")){
                this.lose += 1;
                print = printStatement(botChoice, "won");
            }
            else{
                this.wins += 1;
                print = printStatement(botChoice, "lost");
            }
        }
        else if(playerChoice.equals("Paper")){
            if(botChoice.equals("Scissor")){
                this.lose += 1;
                print = printStatement(botChoice, "won");
            }
            else{
                this.wins += 1;
                print = printStatement(botChoice, "lost");
            }
        }
        else if(playerChoice.equals("Scissor")){
            if(botChoice.equals("Rock")){
                this.lose += 1;
                print = printStatement(botChoice, "won");
            }
            else{
                this.wins += 1;
                print = printStatement(botChoice, "lost");
            }
        }
        else {
            print = "error";
        }
        return print;
    }

    private String printStatement(String botChoice, String condition) {
         
        return "Computer plays "+botChoice+" and the computer "+condition;
    }
}