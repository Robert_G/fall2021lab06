import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

//Robert Gutkowski 1810515
public class RpsApplication extends Application {	
    private RpsGame game;

	public void start(Stage stage) {
		Group root = new Group(); 
        game = new RpsGame();
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
        HBox buttons = new HBox();
        Button b1 = new Button("Rock");
        Button b2 = new Button("Paper");
        Button b3 = new Button("Scissor");
        buttons.getChildren().addAll(b1,b2,b3);

        HBox textFields = new HBox();
        TextField text = new TextField("Welcome!");
        TextField wins = new TextField("wins: "+game.getWins());
        TextField losses = new TextField("losses: "+game.getLose());
        TextField ties = new TextField("ties: "+game.getTies());
        textFields.getChildren().addAll(text, wins, losses, ties);
        text.setPrefWidth(360);
        wins.setPrefWidth(60);
        losses.setPrefWidth(70);
        ties.setPrefWidth(55);
        b1.setOnAction(new RpsChoice(text, wins, losses, ties, b1.getText(), game));
        b2.setOnAction(new RpsChoice(text, wins, losses, ties, b2.getText(), game));
        b3.setOnAction(new RpsChoice(text, wins, losses, ties, b3.getText(), game));
        VBox overall = new VBox();
        overall.getChildren().addAll(buttons,textFields);

        root.getChildren().add(overall);

		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
